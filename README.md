# con-math

Solutions to (some) Concrete Mathematics problems, given in Concrete Mathematics
course at HS ITIS, KFU.

All (or most) of the solutions are written in Go since it outperforms Java and
Python and it's friendlier than C/C++ or Rust.

## Licence

Copyright © 2019 [Nikita Karamov]\
Licensed under the [BSD Zero Clause License].

---

This project is hosted on Codeberg:
<https://codeberg.org/kytta/con-math.git>

[BSD Zero Clause License]: https://spdx.org/licenses/0BSD.html
[Nikita Karamov]: https://www.kytta.dev/
