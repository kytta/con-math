# findmax

Finds the maximum in an array of 100,000,000 random real numbers and finds the
number of assignments made to the `max` variable.

Usage:

```sh
go run findmax NUMBER

# NUMBER - amount of times to run the program
```
